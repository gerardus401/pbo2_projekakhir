-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Des 2019 pada 10.05
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pbo2_projekakhir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `nama` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `posisi` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `gaji` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `alamat` varchar(30) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`nama`, `posisi`, `gaji`, `alamat`) VALUES
('asad', 'Koki', 'Rp. 1.000.000', 'asd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `no_order` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `menu` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `banyaknya` int(10) NOT NULL,
  `harga` int(30) NOT NULL,
  `total_harga` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`no_order`, `menu`, `banyaknya`, `harga`, `total_harga`) VALUES
('1', 'Paket B', 1, 24000, 24000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekapan`
--

CREATE TABLE `rekapan` (
  `sub_total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `no_order` varchar(10) CHARACTER SET armscii8 NOT NULL,
  `no_meja` varchar(10) CHARACTER SET armscii8 NOT NULL,
  `menu` varchar(30) CHARACTER SET armscii8 NOT NULL,
  `banyak` int(10) NOT NULL,
  `harga` int(20) NOT NULL,
  `sub_total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`no_order`, `no_meja`, `menu`, `banyak`, `harga`, `sub_total`) VALUES
('1', '1', 'Paket B', 1, 24000, 24000);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

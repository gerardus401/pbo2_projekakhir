/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_projekakhir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author acer
 */
public class Konfig {
    
    private static Connection MySQLConfog;
    
    public static Connection configDB() throws SQLException{
        try{
            String url= "jdbc:mysql://172.23.10.56:3306/pbo2_projekakhir";
            String user= "root";
            String pass= "";
            
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            MySQLConfog= DriverManager.getConnection(url, user, pass);
            
        }catch (SQLException e){
            System.out.println("Koneksi ke Database Gagal "+ e.getMessage());
            
        }
        return MySQLConfog;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_projekakhir;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author acer
 */
public class Rekapan extends javax.swing.JFrame {

    /**
     * Creates new form Rekapan
     */
    private Object[][] isiTabel = null;

    private String[] header = {"Sub Total"};
    Statement stm;
    Connection Con;
    ResultSet res;
    DefaultTableModel model = new DefaultTableModel();

    /**
     * Creates new form Pemesanan
     */
    private void tampil_data() {

        model.addColumn("Sub Total");

        try {

            String sql = "SELECT * FROM rekapan";
            Connection conn = (Connection) Konfig.configDB();
            stm = conn.createStatement();
            res = stm.executeQuery(sql);

            while (res.next()) {
                model.addRow(new Object[]{res.getString(1)});

            }
            tabelRekapan.setModel(model);
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());

        }
    }

    public void hitungBayar() {

        try {
            int total = 0;
            for (int i = 0; i < tabelRekapan.getRowCount(); i++) {

                String subTotal = (String) tabelRekapan.getValueAt(i, 0);
                int subTot = Integer.parseInt(subTotal);

                total = total + subTot;

            }
            txtPendapatan.setText(String.valueOf(total));

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    private void bacaData() {

        try {

            res = stm.executeQuery("SELECT * FROM rekapan");
            Connection conn = (Connection) Konfig.configDB();
            stm = conn.createStatement();

            ResultSetMetaData meta = res.getMetaData();
            int col = meta.getColumnCount();
            int baris = 0;
            while (res.next()) {
                baris = res.getRow();

            }

            isiTabel = new Object[baris][col];
            int x = 0;
            res.beforeFirst();
            while (res.next()) {
                isiTabel[x][0] = res.getString("sub_total");

                x++;
                 
            }
            tabelRekapan.setModel(new DefaultTableModel(isiTabel, header));
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public Rekapan() {
        initComponents();
        tampil_data();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelRekapan = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtPendapatan = new javax.swing.JTextField();
        tbTampil = new javax.swing.JButton();
        tbKembali = new javax.swing.JButton();
        tbReset = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Rekapan Pendapatan");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabelRekapan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelRekapan);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 86, 467, 123));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 3, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("REKAPAN PENDAPATAN");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 0, 460, 79));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("TOTAL PENDAPATAN");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 216, -1, 28));
        getContentPane().add(txtPendapatan, new org.netbeans.lib.awtextra.AbsoluteConstraints(287, 219, 168, -1));

        tbTampil.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        tbTampil.setText("TAMPIL");
        tbTampil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbTampilActionPerformed(evt);
            }
        });
        getContentPane().add(tbTampil, new org.netbeans.lib.awtextra.AbsoluteConstraints(287, 251, -1, -1));

        tbKembali.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        tbKembali.setText("KEMBALI");
        tbKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(tbKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 314, 92, 38));

        tbReset.setText("RESET");
        tbReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbResetActionPerformed(evt);
            }
        });
        getContentPane().add(tbReset, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 218, -1, -1));

        jPanel4.setBackground(new java.awt.Color(0, 204, 204));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 470, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 4, 470, 60));

        jPanel5.setBackground(new java.awt.Color(0, 0, 102));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 490, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 570, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 4, 490, 570));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tbTampilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbTampilActionPerformed

        bacaData();
        hitungBayar();

        String pesan = "TOTAL PENDAPATAN SAAT INI ADALAH=  ";
        String harga = txtPendapatan.getText();
        double hargaK = Double.parseDouble(harga);
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        JOptionPane.showMessageDialog(null, pesan + kursIndonesia.format(hargaK));


    }//GEN-LAST:event_tbTampilActionPerformed

    private void tbKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbKembaliActionPerformed

        MenuAdmin x = new MenuAdmin();
        x.show();
        dispose();
    }//GEN-LAST:event_tbKembaliActionPerformed

    private void tbResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbResetActionPerformed

        try {
            String sql = "TRUNCATE TABLE rekapan";
            java.sql.Connection conn = (Connection) Konfig.configDB();
            java.sql.PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.execute();
            JOptionPane.showMessageDialog(null, "Reset Data Berhasil...");
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        Rekapan x = new Rekapan();
        x.show();
        dispose();
    }//GEN-LAST:event_tbResetActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Rekapan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelRekapan;
    private javax.swing.JButton tbKembali;
    private javax.swing.JButton tbReset;
    private javax.swing.JButton tbTampil;
    private javax.swing.JTextField txtPendapatan;
    // End of variables declaration//GEN-END:variables
}
